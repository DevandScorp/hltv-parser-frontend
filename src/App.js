import React from 'react';
import ReactNotification from 'react-notifications-component';

import { store } from 'react-notifications-component';
import axios from 'axios';
import * as moment from 'moment';
import 'moment/min/locales';
import preloader from './preloader.gif';
import { API_URL } from './constants';
import './App.css';

class App extends React.Component {

  constructor() {
    super();
    this.state = { days: {}, getEventsPending: false, teams: {} };
  }

  getDaysBetweenDates(startOfWeek, endOfWeek) {
    const weekDuration = endOfWeek.diff(startOfWeek, 'd') + 1;
    const dates = [];
    for (let i = 0; i < weekDuration; ++i) {
      dates.push(startOfWeek.clone().add(i, 'd').format('DD.MM'));
    }
    return dates;
  }

  async componentDidMount() {
    try {
      this.setState({ getEventsPending: true });
      const [locale] = this.getBrowserLocales();
      const startOfWeek = moment().locale(locale || 'ru').startOf('week');
      const endOfWeek = moment().locale(locale || 'ru').endOf('week');
      const weekDays = this.getDaysBetweenDates(startOfWeek, endOfWeek);
      const response = await axios.get(`${API_URL}/api/events?locale=${locale || 'ru'}`);
      this.setState({ days: this.preprocessEvents(response.data, weekDays), getEventsPending: false })
    } catch (err) {
      store.addNotification({
        title: "Error",
        message: "Something went wrong",
        type: "danger",
        insert: "top",
        container: "top-right",
        animationIn: ["animate__animated", "animate__fadeIn"],
        animationOut: ["animate__animated", "animate__fadeOut"],
        dismiss: {
          duration: 2000,
          onScreen: true
        }
      });
      this.setState({ getEventsPending: false });
    }

  }

  preprocessEvents = (events, weekDays) => {
    const days = {};
    for(const weekDay of weekDays) {
      days[weekDay] = [];
    }
    console.log(days);
    if (events && events.length) {
      events.sort((a, b) => new Date(a.date) - new Date(b.date));
      /** Группируем по дням */
      
      events.forEach(event => {
        const day = moment(event.date).format('DD.MM');
        if (days[day]) days[day].push(event);
      });
      return days;
    };
    return [];
  }

  getBrowserLocales = () => {
    const browserLocales =
      navigator.languages === undefined
        ? [navigator.language]
        : navigator.languages;

    if (!browserLocales) {
      return undefined;
    }

    return browserLocales.map(locale => locale.trim());
  }
  render() {
    return (
      <React.Fragment>
        <ReactNotification />
        <div className="app">
          {this.state.getEventsPending ? (
            <div className="d-flex justify-content-center align-items-center preloader">
              <img src={preloader} />
            </div>
          ) :
            (
              <div className="card-wrapper">
                {Object.keys(this.state.days).map((day, index) => (<div className="day-card">
                  <div className="day-card-date" key={index}>
                    <div className="mb-3">{moment(day, 'DD.MM').format('dddd')}</div>
                    <div className="pb-1">{day}</div>
                  </div>
                  {this.state.days[day].map((event, eventIndex) => (
                    <div className="day-card-event" key={`event:${index}:${eventIndex}`}>
                      <div className="event-name">
                        {event.name}
                      </div>
                      <div className="text-center event-time">
                        {moment(event.date).format('HH:mm')}
                      </div>
                      <div className="d-flex justify-content-between event-team align-items-center">
                        <div className="team-info">
                          <img src={event.firstTeam ? event.firstTeam.logo || 'https://static.hltv.org/images/team/logo/10929' : 'https://static.hltv.org/images/team/logo/10929'} />
                          <div>{event.firstTeam ? event.firstTeam.name : '-'}</div>
                        </div>
                        <div>vs</div>
                        <div className="team-info">
                          <img src={event.secondTeam ? event.secondTeam.logo || 'https://static.hltv.org/images/team/logo/10929' : 'https://static.hltv.org/images/team/logo/10929'} />
                          <div>{event.secondTeam ? event.secondTeam.name : '-'}</div>
                        </div>

                      </div>
                      <div className="rank font-weight-bold">Rank</div>
                      <div className="d-flex justify-content-between event-team-rank align-items-center">
                        <div>{event.firstTeam ? event.firstTeam.rank : '-'}</div>
                        <div>{event.secondTeam ? event.secondTeam.rank : '-'}</div>
                      </div>
                    </div>))}
                </div>))}

              </div>

            )}
        </div>
      </React.Fragment>

    );
  }
}

export default App;
